from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional
from starlette.middleware.cors import CORSMiddleware

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# get without using any variables
@app.get("/get_something")
def get_method():
    return {"hello": "world"}


# get with variables
@app.get("/something/{variable}")
def get_variable(variable):
    return {"variable": variable}


# to do POST we need to tell fastapi what to expect
class PostMethod(BaseModel):
    int_var: int
    str_var: str
    optional_var:Optional[str] = None


@app.post("/something/post")
def post_method(post: PostMethod):
    print("hello")
    print(
        post.int_var,
        post.str_var,
        post.optional_var
    )
    return
